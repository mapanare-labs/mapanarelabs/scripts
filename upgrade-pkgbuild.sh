#!/bin/bash

# Variables
REPO="crazy-max/diun"

# Obtener la última versión del paquete desde la API de GitHub
latest_version=$(curl -s "https://api.github.com/repos/$REPO/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | tr -d 'v')

# Obtener la versión actual del archivo PKGBUILD
current_version=$(awk -F= '/^pkgver=/ {print $2}' PKGBUILD)

# Verificar si la versión ha cambiado
if [ "$latest_version" != "$current_version" ]; then
    echo "Se encontró una nueva versión del paquete: $latest_version"
    
    # Actualizar el archivo PKGBUILD con la nueva versión
    sed -i "s/^pkgver=.*/pkgver=$latest_version/" PKGBUILD
    
    echo "La versión en el archivo PKGBUILD ha sido actualizada a $latest_version"
else
    echo "La versión actual del paquete ($current_version) ya está actualizada."
fi
